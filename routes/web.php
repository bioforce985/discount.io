<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function() {
	Route::group(['prefix' => 'admin'], function () {
		Route::group(['middleware' => 'admin:admin'], function() {
			Route::get('sms', 'SmsController@index')->name('sms.list');
			Route::get('sms/birthdate', 'SmsController@setBirthdateText')->name('sms.birthdate');
			Route::post('sms/birthdate', 'SmsController@setBirthdateText');
			Route::get('sms/create', 'SmsController@create')->name('sms.create');
			Route::post('sms/create', 'SmsController@create');
			Route::get('sms/{smsSender}', 'SmsController@create')->name('sms.show');
			Route::get('card/{card}/sms', 'CardController@sms')->name('card.sms');
			Route::post('card/{card}/sms', 'CardController@sms');
			Route::get('vip/{vip}/sms', 'VipController@checks')->name('vip.sms');
			Route::post('vip/{vip}/sms', 'VipController@checks');
			Route::resources([
				'user' => 'UserController'
			]);
		});
		Route::group(['middleware' => 'admin:manager'], function() {
			Route::get('card/{card}/checks', 'CardController@checks')->name('card.checks');
			Route::get('card/{card}/checks/add', 'CardController@checksAdd')->name('card.checks.add');
			Route::post('card/{card}/checks/add', 'CardController@checksAdd');
			//Route::get('card/migrate', 'CardController@migrate');
			//Route::post('card/migrate', 'CardController@migrate');

			Route::resources([
				'check' => 'CheckController',
				'card' => 'CardController',
				'vip' => 'VipController'
			]);
		});
	});
});