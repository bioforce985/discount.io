@extends('layouts.app')

@section('content')
<div class="row">
    
    <div class="col-md-8 col-md-offset-2">
        <form method="get" enctype="multipart/form-data">
            <input type="hidden" name="set_filter" value="y">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Дата</label>
                    <input type="text" class="form-control" name="date_start" placeholder="с" id="datetimepicker1" value="{{request()->input('date_start')}}" autocomplete="off">
                </div>
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <input type="text" class="form-control" name="date_end" placeholder="по" id="datetimepicker2" value="{{request()->input('date_end')}}" autocomplete="off">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary form-control" name="search" value="y">Применить</button>
                </div>
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <a href="/admin/check" class="btn btn-info form-control">Очистить</a>
                </div>
            </div>
        </form>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Сумма</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Покупатель</th>
                    <th scope="col">Из файла</th>
                    <th scope="col">Скидка</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($checks as $check)
                <tr>
                    <th scope="row">{{$check->id}}</th>
                    <td>{{$check->sum}}</td>
                    <td>{{$check->buyed_on}}</td>
                    <td>{{$check->card->name}}</td>
                    <td>
                        @if ($check->initial)
                        Да
                        @endif
                    </td>
                    <td>
                        @if ($check->is_discount)
                        Да
                        @endif
                    </td>
                    <td>
                        @if (!$check->is_discount && !$check->initial)
                        <a href="#" onclick="event.preventDefault();
                            if(confirm('Действительно удалить?'))
                                document.getElementById('delete-check-manager-form-{{$check->id}}').submit();">Удалить</a>
                            <form action="{{ route('check.destroy', ['check' => $check]) }}" method="POST" id="delete-check-manager-form-{{$check->id}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $checks->links() }}
    </div>
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    });
</script>
@endsection