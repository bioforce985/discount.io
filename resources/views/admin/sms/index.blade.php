@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-2">
        <a class="btn btn-primary" href="{{route('sms.create')}}" role="button">Добавить рассылку</a>
    </div>
    <div class="col-md-2">
        <a class="btn btn-primary" href="{{route('sms.birthdate')}}" role="button">Изменить текст для ДР</a>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Рассылка</th>
                    <th scope="col">Всего</th>
                    <th scope="col">Отправлено</th>
                    <th scope="col">Ошибок</th>
                    <th scope="col">Пользователь</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($smsSenders as $smsSender)
                <tr>
                    <th scope="row">{{$smsSender->id}}</th>
                    <td><a href="{{route('sms.show', ['smsSender' => $smsSender])}}">{{$smsSender->name}}</td>
                    <td>{{$smsSender->all}}</td>
                    <td>{{$smsSender->sended}}</td>
                    <td>{{$smsSender->errors}}</td>
                    <td>{{$smsSender->user->name}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $smsSenders->links() }}
    </div>
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM'
        });
    });
</script>
@endsection
