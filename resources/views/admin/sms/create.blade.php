@extends('layouts.app')

@section('content')
<div class="row">
    
    <div class="col-md-8 col-md-offset-2">

        <div class="" id="">
            <form method="get" enctype="multipart/form-data">
            <div class="form-row">
                <div class="form-group col-md-12">
                <h2>Добавить рассылку</h2>
                </div>
            </div>

            <input type="hidden" name="set_filter" value="y">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Скидка</label>
                    <input type="text" class="form-control" name="percent_rate" placeholder="Скидка" value="{{request()->input('percent_rate')}}">
                </div>
                <div class="form-group col-md-3">
                    <label>Дата рождения</label>
                    <input type="text" class="form-control" name="date_start" placeholder="с" id="datetimepicker1" value="{{request()->input('date_start')}}" autocomplete="off">
                </div>
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <input type="text" class="form-control" name="date_end" placeholder="по" id="datetimepicker2" value="{{request()->input('date_end')}}" autocomplete="off">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary form-control" name="search" value="y">Найти</button>
                </div>
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <a href="/admin/sms" class="btn btn-info form-control">Вернуться</a>
                </div>
                <div class="form-group col-md-6">
                </div>
            </div>
            </form>
        </div>
    </div>
    @if ($cards)
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Дата рождения</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Процент</th>
                    <th scope="col">Сумма</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cards as $card)
                <tr>
                    <th scope="row">{{$card->id}}</th>
                    <td><a href="{{route('card.show', ['id' => $card->id])}}">{{$card->name}}</td>
                    <td>{{$card->birthdate}}</td>
                    <td>{{$card->phone}}</td>
                    <td>{{$card->show_percent}}</td>
                    <td>{{$card->checks()->sum('sum')/100}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $cards->appends(Request::input())->links() }}
        <form method="post">
            {{ csrf_field() }}
            @foreach (Request::except('search') as $key => $input)
            <input type="hidden" name="{{$key}}" value="{{$input}}">
            @endforeach
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Название рассылки</label>
                    <input type="text" class="form-control" name="name" placeholder="Название рассылки" value="">
                </div>
                <div class="form-group col-md-6">
                    <label>Текст</label>
                    <input type="text" class="form-control" name="text" placeholder="Текст" value="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary form-control" name="send" value="y">Отправить</button>
                </div>
                <div class="form-group col-md-3">
                </div>
                <div class="form-group col-md-6">
                </div>
            </div>
        </form>
    </div>
    @endif

    
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM'
        });
    });
</script>
@endsection
