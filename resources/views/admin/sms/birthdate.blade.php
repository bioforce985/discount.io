@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Изменить текст SMS для дней рождений</div>

            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Введите текст</label>
                        <textarea type="text" class="form-control" id="exampleFormControlTextarea1" name="text">{{$birthdateText}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Изменить</button>
                    <a href="{{route('sms.list')}}" class="btn btn-warning">Вернуться в рассылки SMS</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
