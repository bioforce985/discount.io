@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Добавить карту</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('card.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Имя:</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                        <label for="datetimepicker1" class="col-md-4 control-label">День рождения:</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="birthdate" value="{{ old('birthdate') }}" id="datetimepicker1">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-4 control-label">Телефон:</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="375**********">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('sum') ? ' has-error' : '' }}">
                        <label for="sum" class="col-md-4 control-label">Сумма покупки:</label>

                        <div class="col-md-6">
                            <input id="sum" type="text" class="form-control" name="sum" value="{{ old('sum') }}">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('sum') ? ' has-error' : '' }}">
                        <label for="percent" class="col-md-4 control-label">Скидка:</label>

                        <div class="col-md-6">
                            <select name="percent" class="form-control">
                                <option value="">Без скидки</option>
                                <option value="5">5%</option>
                                <option value="7">7%</option>
                                <option value="10">10%</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label for="sms_send" class="">
                                    <input id="sms_send" type="checkbox" name="sms_send" value="1" checked="checked">Отправлять SMS
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Добавить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM'
        });
    });
</script>
@endsection
