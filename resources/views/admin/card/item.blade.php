@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$card->name}}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class=" col-md-9 col-lg-9 "> 
                        <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td>День рождения:</td>
                                <td>{{$card->birthdate}}</td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td>{{$card->phone}}</td>
                            </tr>
                            <tr>
                                <td>Скидка</td>
                                <td>{{$card->show_percent}}</td>
                            </tr>
                            <tr>
                                <td>Сумма покупок</td>
                                <td><a href="{{route('card.checks', ['card' => $card])}}">{{$card->checks()->sum('sum')/100}}</a></td>
                            </tr>
                        </tbody>
                        </table>

                        <a href="{{route('card.checks.add', ['card' => $card])}}" class="btn btn-primary">Добавить чек</a>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                @if (Auth::user()->checkRole('admin'))
                <a href="{{route('card.sms', ['card' => $card])}}" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                @endif
                <span class="pull-right">
                <a href="#" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();
                                                    if(confirm('Действительно удалить?'))
                                                     document.getElementById('delete-card-form').submit();"><i class="glyphicon glyphicon-trash"></i></a>
                <a href="{{route('card.edit', ['card' => $card])}}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                </span>
                <form action="{{route('card.destroy', ['card' => $card])}}" method="POST" id="delete-card-form">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
