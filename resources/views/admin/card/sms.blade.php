@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Отправить SMS для <a href="{{route('card.show', ['card' => $card])}}">{{$card->name}}</a></div>

            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Введите текст</label>
                        <textarea type="text" class="form-control" id="exampleFormControlTextarea1" name="text"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                    <a href="{{route('card.show', ['card' => $card])}}" class="btn btn-warning">Вернуться в карточку</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
