@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$card->name}}</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('card.update', ['card' => $card]) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Имя:</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $card->name }}">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="datetimepicker1" class="col-md-4 control-label">День рождения:</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="birthdate" value="{{ $card->birthdate }}" id="datetimepicker1">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-4 control-label">Телефон:</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ $card->phone }}" placeholder="375**********">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="percent" class="col-md-4 control-label">Скидка:</label>

                        <div class="col-md-6">
                            <input id="percent" type="number" class="form-control" name="percent" value="{{ $card->show_percent }}" max="20">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label for="sms_send" class="">
                                    <input id="sms_send" type="checkbox" name="sms_send" value="1" {{ ($card->sms_send)?'checked="checked"':'' }}>Отправлять SMS
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Изменить
                            </button>
                            <a href="{{route('card.show', ['card' => $card])}}" class="btn btn-secondary">Отменить</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM'
        });
    });
</script>
@endsection
