@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <a href="{{route('card.show', ['card' => $card])}}" class="btn btn-warning">Вернуться в карточку</a>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Сумма</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Из файла</th>
                    <th scope="col">Скидка</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($checks as $check)
                <tr>
                    <th scope="row">{{$check->id}}</th>
                    <td>{{$check->sum}}</td>
                    <td>{{$check->buyed_on}}</td>
                    <td>
                        @if ($check->initial)
                        Да
                        @endif
                    </td>
                    <td>
                        @if ($check->is_discount)
                        Да
                        @endif
                    </td>
                    <td>
                        @if (!$check->is_discount && !$check->initial)
                        <a href="#" onclick="event.preventDefault();
                            if(confirm('Действительно удалить?'))
                                document.getElementById('delete-check-manager-form-{{$check->id}}').submit();">Удалить</a>
                            <form action="{{ route('check.destroy', ['check' => $check]) }}" method="POST" id="delete-check-manager-form-{{$check->id}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $checks->appends(Request::input())->links() }}
    </div>
</div>
@endsection
