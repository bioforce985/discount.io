@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Добавить покупку для <a href="{{route('card.show', ['card' => $card])}}">{{$card->name}}</a></div>

            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Сумма</label>
                        <input type="text" class="form-control-file" id="exampleFormControlFile1" name="sum">
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                    <a href="{{route('card.show', ['card' => $card])}}" class="btn btn-warning">Вернуться в карточку</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
