@extends('layouts.app')

@section('content')
<div class="row">
    
    <div class="col-md-8 col-md-offset-2">
        
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Администратор</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if ($user->checkRole('admin'))
                            Администратор<br/>
                            <a href="#" onclick="event.preventDefault();
                            if(confirm('Действительно удалить?'))
                                document.getElementById('edit-card-form-{{$user->id}}').submit();">Удалить из менеджеров и админов</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="0">
                            </form>
                            <a href="#" onclick="event.preventDefault();
                                document.getElementById('edit-card-manager-form-{{$user->id}}').submit();">Сделать менеджером</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-manager-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="1">
                            </form>
                        @elseif($user->checkRole('manager'))
                            Менеджер<br/><a href="#" onclick="event.preventDefault();
                                document.getElementById('edit-card-form-{{$user->id}}').submit();">Сделать админом</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="2">
                            </form>
                            <a href="#" onclick="event.preventDefault();
                            if(confirm('Действительно удалить?'))
                                document.getElementById('edit-card-manager-form-{{$user->id}}').submit();">Удалить из менеджеров</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-manager-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="0">
                            </form>
                        @else
                            <a href="#" onclick="event.preventDefault();
                                document.getElementById('edit-card-form-{{$user->id}}').submit();">Сделать админом</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="2">
                            </form>
                            <a href="#" onclick="event.preventDefault();
                                document.getElementById('edit-card-manager-form-{{$user->id}}').submit();">Сделать Менеджером</a>
                            <form action="{{ route('user.update', ['user' => $user]) }}" method="POST" id="edit-card-manager-form-{{$user->id}}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input id="is_admin-{{$user->id}}" type="hidden" class="form-control" name="is_admin" value="1">
                            </form>
                        @endif
                    </td>
                    <td>
                        @if (Auth::user()->id != $user->id)
                        <a href="#" data-original-title="Delete this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault();
                                                if(confirm('Действительно удалить?'))
                                                     document.getElementById('delete-card-form-{{$user->id}}').submit();"><i class="glyphicon glyphicon-trash"></i></a>
                        <form action="{{route('user.destroy', ['user' => $user])}}" method="POST" id="delete-card-form-{{$user->id}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection