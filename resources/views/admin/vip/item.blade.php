@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$vip->name}}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class=" col-md-9 col-lg-9 "> 
                        <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td>День рождения:</td>
                                <td>{{$vip->birthdate}}</td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td>{{$vip->phone}}</td>
                            </tr>
                            <tr>
                                <td>Скидка</td>
                                <td>{{$vip->percent}}</td>
                            </tr>
                        </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <!--<a href="{{route('vip.sms', ['vip' => $vip])}}" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>-->
                <span class="pull-right">
                <a href="{{route('vip.edit', ['vip' => $vip])}}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                </span>
            </div>
        </div>
    </div>
</div>
@endsection
