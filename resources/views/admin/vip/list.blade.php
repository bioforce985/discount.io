@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <a class="badge badge-primary" href="#filterCollaps" data-toggle="collapse" role="button" @if (request()->input('set_filter')) aria-expanded="true" @else aria-expanded="false" @endif aria-controls="filterCollaps">
            Показать фильтр
        </a>
        <a class="btn btn-primary" href="{{route('vip.create')}}" role="button">Добавить карту</a>
        <div class="collapse @if (request()->input('set_filter')) show @endif" id="filterCollaps">
            <form method="get" enctype="multipart/form-data">
                <input type="hidden" name="set_filter" value="y">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Имя</label>
                        <input type="text" class="form-control" name="name" placeholder="Имя" value="{{request()->input('name')}}">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Телефон</label>
                        <input type="text" class="form-control" name="phone" placeholder="Телефон" value="{{request()->input('phone')}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Дата рождения </label>
                        <input type="text" class="form-control" name="date_start" placeholder="с" id="datetimepicker1" value="{{request()->input('date_start')}}" autocomplete="off">
                    </div>
                    <div class="form-group col-md-3">
                        <label>&nbsp;</label>
                        <input type="text" class="form-control" name="date_end" placeholder="по" id="datetimepicker2" value="{{request()->input('date_end')}}" autocomplete="off">
                    </div>
                    <div class="form-group col-md-3">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary form-control">Найти</button>
                    </div>
                    <div class="form-group col-md-3">
                        <label>&nbsp;</label>
                        <a href="/admin/vip" class="btn btn-info form-control">Очистить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Дата рождения</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Процент</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vips as $vip)
                <tr>
                    <th scope="row">{{$vip->id}}</th>
                    <td><a href="{{route('vip.show', ['id' => $vip->id])}}">{{$vip->name}}</td>
                    <td>{{$vip->birthdate}}</td>
                    <td>{{$vip->phone}}</td>
                    <td>{{$vip->percent}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $vips->links() }}
    </div>
</div>
@endsection

@section('adittional-scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM'
        });
    });
</script>
@endsection
