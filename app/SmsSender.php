<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSender extends Model
{
    //
    /**
     *
     * Eloquent Relationships
     *
     *
    */
    protected $fillable = [
        'name', 'text', 'all', 'sended', 'user_id', 'errors'
    ];

    public function sms()
    {
        return $this->hasMany('App\Sms');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
