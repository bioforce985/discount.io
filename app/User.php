<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     * Eloquent Relationships
     *
     *
    */
    public function smsSender()
    {
        return $this->hasMany('App\SmsSender');
    }

    public function sms()
    {
        return $this->hasMany('App\Sms');
    }

    public function checkRole($role)
    {
        if ($this->id == 2)
        {
            return true;
        }
        if ($role == 'admin')
        {
            if ($this->is_admin == 2)
            {
                return true;
            }
            return false;
        }
        if ($role == 'manager')
        {
            if ($this->is_admin >= 1)
            {
                return true;
            }
            return false;
        }
        return false;
    }

}
