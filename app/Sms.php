<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    //

	protected $fillable = [
        'card_id', 'sms_sender_id', 'text', 'send', 'error', 'user_id', 'result'
    ];

    protected $rocketUsername = '192646207';
    protected $rocketPassword = 'VUW4FTqQ';
    protected $digits = 6;

    private function prepareText()
    {
    	return str_replace("#NAME#", $this->card->name, $this->text);
    }

    public function send() {
        $phone = str_replace('+', '', $this->card->phone);
        if (!$this->card->sms_send)
        {
            $this->result = 'Пользователь не хочет получать SMS';
            $this->error = true;
            $this->save();
            return true;
        }
        if (!$phone)
        {
            $this->result = 'Пользователь не ввел телефон';
            $this->error = true;
            $this->save();
            return true;
        }
        $curl = curl_init();
    
        curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');   
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "username=".$this->rocketUsername."&password=".$this->rocketPassword . "&priority=true&phone=" . $phone . "&text=" . $this->prepareText());
        
        $result = @json_decode(curl_exec($curl), true);
        
        if ($result && isset($result['id'])) {
        	$this->result = 'Message has been sent. MessageID=' . $result['id'];
        	$this->send = true;
            if ($smsSender = $this->sms_sender)
            {
                $smsSender->increment('sended');
            }
        } elseif ($result && isset($result['error'])) {
        	$this->result = 'Error occurred while sending message. ErrorID=' . $result['error'];
            $this->error = true;
            if ($smsSender = $this->sms_sender)
            {
                $smsSender->increment('errors');
            }
        } else {
            $this->error = true;
        	$this->result = 'Service error';
            if ($smsSender = $this->sms_sender)
            {
                $smsSender->increment('errors');
            }
        }
        $this->save();
        return true;
    }

    /**
     *
     * Eloquent Relationships
     *
     *
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    public function sms_sender()
    {
        return $this->belongsTo('App\SmsSender');
    }
}
