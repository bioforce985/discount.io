<?php

namespace App\Http\Controllers;

use App\Vip;
use Illuminate\Http\Request;

class VipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vips = new Vip;
        if ($request->set_filter)
        {

            $vips = $vips->filter($request->all())->paginate(20);
        }
        else
        {
            $vips = $vips->paginate(20);
        }
        return view('admin.vip.list', ['vips' => $vips]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vip.create', []);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'phone' => 'nullable|unique:vips|max:13',
        ]);
        $vip = new Vip($request->all());
        $vip->save();

        return redirect()->route('vip.index')->with('status', 'VIP Карта добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vip  $vip
     * @return \Illuminate\Http\Response
     */
    public function show(Vip $vip)
    {
        return view('admin.vip.item', ['vip' => $vip]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vip  $vip
     * @return \Illuminate\Http\Response
     */
    public function edit(Vip $vip)
    {
        return view('admin.vip.edit', ['vip' => $vip]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vip  $vip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vip $vip)
    {
        if ($request->isMethod('put'))
        {
            //echo "<pre>"; print_r()
            $vip->update($request->only(['birthdate', 'phone', 'percent']));
            return back()->with('status', 'Изменения сохранены!');
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vip  $vip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vip $vip)
    {
        //
    }
}
