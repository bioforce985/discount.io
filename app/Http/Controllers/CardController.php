<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use Excel;
use Validator;
use Carbon\Carbon;
use Auth;
use App\Jobs\SendOneSms;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $cards = new Card;
        if ($request->set_filter)
        {

            $cards = $cards->filter($request->all())->paginate(20);
        }
        else
        {
            $cards = $cards->paginate(20);
        }
        return view('admin.card.list', ['cards' => $cards]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.card.create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'old_id' => 'numeric',
            'name' => 'required',
            'phone' => 'unique:cards|max:13',
        ]);
        $card = new Card($request->all());
        $card->save();
        if ($request->sum > 0)
        {
            $card->addCheck($request->sum);
        }
        if ($request->percent)
        {
            $card->percent = $request->percent;
            if (!$request->sum)
            {
                $card->addPercentCheck($request->percent);
            }
        }
        $card->save();
        
        return redirect()->route('card.show', ['card' => $card])->with('status', 'Карта добавлена');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        return view('admin.card.item', ['card' => $card]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Card $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        return view('admin.card.edit', ['card' => $card]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        if ($request->isMethod('put'))
        {
            $card->update($request->only(['name', 'birthdate', 'phone', 'percent', 'old_id']));
            $card->update(['sms_send' => $request->sms_send?true:false]);
            return back()->with('status', 'Изменения сохранены!');
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        //
        $card->delete();
        return redirect()->route('card.index')->with('status', 'Карта удалена');
    }

    /**
     * Show checks of the card
     *
     * @param  Card $card
     * @return \Illuminate\Http\Response
     */
    public function checks(Card $card, Request $request)
    {
        return view('admin.card.check.list', ['card' => $card, 'checks' => $card->checks()->paginate(10)]);
        //
    }
    
    /**
     * Add check to the card
     *
     * @param  Card $card
     * @return \Illuminate\Http\Response
     */
    public function checksAdd(Card $card, Request $request)
    {
        if ($request->isMethod('post'))
        {
            if ($request->sum > 0)
            {
                $card->addCheck($request->sum);
                return redirect()->route('card.show', ['card' => $card])->with('status', 'Чек добавлен!');
            }            
        }
        return view('admin.card.check.add', ['card' => $card]);
        //
    }

    /**
     * Migrate cards from excel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function migrate(Request $request)
    {
        if ($request->isMethod('post')) {
            //
            $data = Excel::load($request->excel, function($reader) {

            })->get();
            //echo "<pre>"; print_r($data); echo "</pre>";
            //exit(); die();
            foreach ($data as &$row)
            {
                $createFields = $row->except(['summa', '0'])->toArray();
                $createFields['percent'] = str_replace('%', '', $createFields['percent']);
                $createFields['percent'] = str_replace(',', '.', $createFields['percent']);
                if ($createFields['percent'] < 1 && $createFields['percent']>0)
                {
                    $createFields['percent'] = $createFields['percent']*100;
                }
                if (!$createFields['percent'])
                {
                    unset($createFields['percent']);
                }
                $card = Card::create($createFields);
                if ($row->summa && $card)
                {
                    $summa = str_replace(',', '.', $row->summa);
                    $card->checks()->create(['sum' => $summa*100, 'initial' => true, 'buyed_on' => Carbon::now()]);
                }
            }
            return "1";
        }
        else
        {
            return view('admin.card.import');
        }
    }

    /**
     * Add check to the card
     *
     * @param  Card $card
     * @return \Illuminate\Http\Response
     */
    public function sms(Card $card, Request $request)
    {
        if ($request->isMethod('post'))
        {
            if (!$request->text)
            {
                return back()->with('error', 'Введите текст смс');
            }
            
            $sms = $card->sms()->create(['text' => $request->text]);
            $sms->user()->associate(Auth::user());
            $sms->save();
            $job = (new SendOneSms($sms))
                ->onConnection('database');
            dispatch($job);
            return redirect()->route('card.show', ['card' => $card])->with('status', "SMS отправлено");

            if (isset($return['error']))
            {
                return redirect()->route('card.show', ['card' => $card])->with('error', $return['error']);
            }

            return redirect()->route('card.show', ['card' => $card])->with('error', 'Что то пошло не так');
        }
        return view('admin.card.sms', ['card' => $card]);
        //
    }
}
