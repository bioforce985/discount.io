<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\Sms;
use App\SmsSender;
use Auth;
use App\Jobs\SendSmsSending;

class SmsController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $smsSenders = new SmsSender;
        $smsSenders = $smsSenders->paginate(20);

        return view('admin.sms.index', ['smsSenders' => $smsSenders]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if ($request->isMethod('get') && $request->set_filter)
        {
            $cards = new Card;
            if ($request->search == 'y')
            {
                $cards = $cards->filter($request->all())->paginate(20);
                return view('admin.sms.create', ['cards' => $cards]);
            }
            return view('admin.sms.create', ['cards' => $cards]);
        }
        if ($request->isMethod('post') && $request->send == 'y')
        {
            $cards = new Card;
            $cardIds = $cards->select('id')->filter($request->except('name'))->get()->toArray();

            $createSmsSender = ['name' => $request->name, 'text'=> $request->text, 'all' => count($cardIds), 'sended' => 0];

            $smsSender = Auth::user()->smsSender()->create($createSmsSender);

            $job = (new SendSmsSending($cardIds, $smsSender))
                ->onConnection('database');
            dispatch($job);
            return redirect()->route('sms.list')->with('status', "SMS рассылка добавлена");
        }
        $smsSender = new SmsSender;
        $smsSender = $smsSender->paginate(20);

        return view('admin.sms.create', ['cards' => false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        //return redirect()->route('card.show', ['card' => $card])->with('status', 'Карта добавлена');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SmsSender $smsSender)
    {
        return view('admin.card.item', ['smsSender' => $smsSender]);
        //
    }

    public function prepareForShowBirthdateText($text)
    {
        return str_replace('%$%', ' ', $text);
    }

    public function prepareForSetBirthdateText($text)
    {
        return str_replace(' ', '%$%', $text);
    }

    public function setBirthdateText(Request $request)
    {
        if (!$request->isMethod('post'))
        {
            return view('admin.sms.birthdate', ['birthdateText' => $this->prepareForShowBirthdateText(config('app.birthdate'))]);
        }
        $envValue = $request->text;
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        $str = str_replace(config('app.birthdate'), $this->prepareForSetBirthdateText($envValue), $str);

        $fp = fopen($envFile, 'w');
        fwrite($fp, $str);
        fclose($fp);
        return back()->with('status', "Текст SMS изменен");
    }

    public function setBirthdateSms(Request $request)
    {

    }
}
