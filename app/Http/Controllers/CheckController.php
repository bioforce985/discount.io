<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Check;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $checks = new Check;
        $checks = $checks->where('initial', '!=', true);
        if ($request->set_filter)
        {

            $checks = $checks->filter($request->all())->paginate(20);
        }
        else
        {
            $checks = $checks->paginate(20);
        }
        return view('admin.check.list', ['checks' => $checks]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Check $check)
    {
        //
        $card = $check->card;
        $check->delete();
        $card->resumPercent();
        return redirect()->back()->with('status', 'Чек удален');
    }
}
