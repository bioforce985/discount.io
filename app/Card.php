<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Card extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'old_id', 'name', 'birthdate', 'phone', 'percent', 'percent_custom', 'sms_send'
    ];
    
    /**
     *
     *
     * Scopes
     *
     */

    public function scopeFilter($query, $filter)
    {
        if (isset($filter['percent_rate']) && $filter['percent_rate'])
        {
            $query->where('percent_custom', $filter['percent_rate'])->orWhere('percent', $filter['percent_rate']);
        }
        
        if (isset($filter['name']) && $filter['name'])
        {
            $query->where('name', 'like', '%'.$filter['name'].'%');
        }

        if (isset($filter['percent']) && $filter['percent'])
        {
            $query->where('percent', $filter['percent']);
        }

        if (isset($filter['percent_custom']) && $filter['percent_custom'])
        {
            $query->where('percent_custom', $filter['percent_custom']);
        }

        if (isset($filter['old_id']) && $filter['old_id'])
        {
            $filtersIds = explode(",", $filter['old_id']);
            $query->whereIn('old_id', $filtersIds);
        }

        if (isset($filter['id']) && $filter['id'])
        {
            $filtersIds = explode(",", $filter['id']);
            $query->whereIn('id', $filtersIds);
        }

        if (isset($filter['date_start']) && $filter['date_start'] && isset($filter['date_end']) && $filter['date_end'])
        {
            $startToArray = Carbon::createFromFormat("d/m", $filter['date_start']);
            $endToArray = Carbon::createFromFormat("d/m", $filter['date_end']);
            if ($startToArray >= $endToArray)
            {
                $datRange = explode("/", $filter['date_start']);
                $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
            }
            else
            {
                $isFirstD = true;
                while ($startToArray<=$endToArray)
                {
                    if ($isFirstD)
                    {
                        $birthdate = $startToArray->format('d/m');
                        $isFirstD = false;
                        $query->where(function ($query) use ($birthdate){
                            $datRange = explode("/", $birthdate);
                            $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
                        });
                    }
                    else
                    {
                        $birthdate = $startToArray->format('d/m');
                        $isFirstD = false;
                        $query->orWhere(function ($query) use ($birthdate){
                            $datRange = explode("/", $birthdate);
                            $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
                        });
                    }
                    $startToArray->addDay();
                }
            }
            
        }
        elseif (isset($filter['date_start']) && $filter['date_start'])
        {
            $datRange = explode("/", $filter['date_start']);
            $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
        }

        if (isset($filter['phone']) && $filter['phone'])
        {
            $query->where('phone', 'like', '%'.$filter['phone'].'%');
        }
        return $query;
    }

    /**
     *
     * Eloquent Relationships
     *
     *
    */
    public function checks()
    {
        return $this->hasMany('App\Check');
    }

    public function sms()
    {
        return $this->hasMany('App\Sms');
    }

    /**
    *
    *Mutators
    *
    */

    public function getShowPercentAttribute()
    {
        return ($this->percent_custom > $this->percent)?$this->percent_custom:$this->percent;
    }

    public function getBirthdateAttribute($value)
    {
        if (!$value)
        {
            return '';
        }
        $value = new Carbon($value);

        return $value->format('d/m');
    }

    public function setBirthdateAttribute($value)
    {
        if ($value && strlen($value)<=5)
        {
            $this->attributes['birthdate'] = Carbon::createFromFormat('Y/d/m', date('Y').'/'.$value);
        }
        elseif ($value)
        {
            $this->attributes['birthdate'] = new Carbon($value);
        }
    }

    public function addPercentCheck($percent)
    {
        $sum = false;
        if ($percent == 5)
        {
            $sum = 300;
        }
        if ($percent == 7)
        {
            $sum = 1000;
        }
        if ($percent == 10)
        {
            $sum = 2000;
        }
        if ($sum)
        {
            $this->checks()->create(['sum' => $sum*100, 'initial' => false, 'is_discount' => true, 'buyed_on' => Carbon::now()]);
        }
    }

    public function addCheck($sum)
    {        
        $percent = $this->show_percent;
        $sum = str_replace(",", ".", $sum);
        $this->checks()->create(['sum' => $sum*100, 'initial' => false, 'buyed_on' => Carbon::now()]);
        if ($sum >= 300 && $percent < 5)
        {
            $percent = 5;
        }
        if ($sum >= 400 && $percent < 7)
        {
            $percent = 7;
        }

        if ($sum >= 600 && $percent >= 10 && $percent < 20)
        {
            $percent = 20;
        }

        if ($sum >= 600 && $percent < 10)
        {
            $percent = 10;
        }
        $sum7Percent = 0;
        if ($percent == 7 && $onPercentCheck = $this->checks()->where('sum', '>=', 40000)->first())
        {
            $sum7Percent = $this->checks()->where('id', '>', $onPercentCheck->id)->sum('sum')/100;
        }
        $sumChecks = $this->checks()->sum('sum')/100;
        if ($sumChecks >= 300 && $percent < 5)
        {
            $percent = 5;

        }
        if ($sumChecks >= 1000 && $percent < 7)
        {
            $percent = 7;

        }
        if ($sum7Percent >= 1000 && $percent == 7)
        {
            $percent = 10;

        }
        if ($sumChecks >= 2000 && $percent < 10)
        {
            $percent = 10;

        }
        if ($percent > $this->show_percent)
        {
            $this->percent = $percent;
            $this->save();
        }
        return true;
    }

    public function resumPercent()
    {
        $percent = $this->show_percent;
        $sumChecks = $this->checks()->sum('sum')/100;
        if ($sumChecks < 2000 && $percent >= 10)
        {
            $percent = 7;

        }
        if ($sumChecks < 1000 && $percent >= 7)
        {
            $percent = 5;

        }
        if ($sumChecks < 300 && $percent >= 5)
        {
            $percent = 0;

        }
        $this->percent = $percent;
        $this->save();
    }
}
