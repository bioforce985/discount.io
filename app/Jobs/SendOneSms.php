<?php

namespace App\Jobs;

use App\User;
use App\Sms;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendOneSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $Sms;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Sms $Sms)
    {
        //
        $this->Sms = $Sms;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $Sms = $this->Sms;
        if ($Sms->send())
        {
            return true;
        }
        return true;
    }
}
