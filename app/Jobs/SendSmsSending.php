<?php

namespace App\Jobs;

use App\Card;
use App\SmsSender;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\SendOneSms;

class SendSmsSending implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $cardIds;
    protected $SmsSender;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cardIds, SmsSender $SmsSender)
    {
        $this->cardIds = $cardIds;
        $this->SmsSender = $SmsSender;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cardIds = $this->cardIds;

        $User = $this->SmsSender->user;
        if (!is_array($cardIds))
        {
            $cardIds = [$cardIds];
        }

        foreach ($cardIds as $cardId)
        {
            $card = Card::where('id', $cardId)->firstOrFail();
            $sms = $card->sms()->create(['text' => $this->SmsSender->text]);
            $sms->user()->associate($User);
            $sms->sms_sender()->associate($this->SmsSender);
            $sms->save();
            $job = (new SendOneSms($sms))
                ->onConnection('database');
            dispatch($job);
        }
        return true;
        //
    }
}
