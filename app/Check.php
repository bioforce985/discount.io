<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Check extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sum', 'initial', 'buyed_on', 'is_discount'
    ];

    /**
     *
     * Eloquent Relationships
     *
     *
    */
    //Cards
    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    /**
     *
     * Mutators
     *
     *
    */
    //Summ with demitial
    public function getSumAttribute($value)
    {
        return $value/100;
    }

    public function scopeFilter($query, $filter)
    {
        if (isset($filter['date_start']) && $filter['date_start'] && isset($filter['date_end']) && $filter['date_end'])
        {
            $filter['date_start'] = $filter['date_start']. ' 00:00:00';
            $filter['date_end'] = $filter['date_end']. ' 00:00:00';
            $query->where('created_at', '>=', Carbon::createFromFormat("d/m/Y H:i:s", $filter['date_start']));
            $query->where('created_at', '<=', Carbon::createFromFormat("d/m/Y H:i:s", $filter['date_end'])->addDay());            
        }
        elseif (isset($filter['date_start']) && $filter['date_start'])
        {
            $filter['date_start'] = $filter['date_start']. ' 00:00:00';
            $query->where('created_at', '>=', Carbon::createFromFormat("d/m/Y H:i:s", $filter['date_start']));
            $query->where('created_at', '<=', Carbon::createFromFormat("d/m/Y H:i:s", $filter['date_start'])->addDay());
        }

        return $query;
    }
}
