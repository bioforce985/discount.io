<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Vip extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birthdate', 'phone', 'percent'
    ];

    /**
     *
     *
     * Scopes
     *
     */

    public function scopeFilter($query, $filter)
    {
        if (isset($filter['name']) && $filter['name'])
        {
            $query->where('name', 'like', '%'.$filter['name'].'%');
        }

        //print_r($filter);
        if ($filter['date_start'] && $filter['date_end'])
        {
            $datRange = explode("/", $filter['date_start']);
            $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
        }
        elseif (isset($filter['date_start']) && $filter['date_start'])
        {
            $datRange = explode("/", $filter['date_start']);
            $query->whereMonth('birthdate', $datRange[1])->whereDay('birthdate', $datRange[0]);
        }

        if (isset($filter['phone']) && $filter['phone'])
        {
            $query->where('phone', 'like', '%'.$filter['phone'].'%');
        }
        return $query;
    }


    public function getBirthdateAttribute($value)
    {
        if (!$value)
        {
            return '';
        }
        $value = new Carbon($value);

        return $value->format('m/d');
    }

    public function setBirthdateAttribute($value)
    {
        if ($value)
        {
            $this->attributes['birthdate'] = Carbon::createFromFormat('Y/m/d', date('Y').'/'.$value);
        }
    }
}
